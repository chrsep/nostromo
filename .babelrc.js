const presets = [
  "@babel/preset-env",
  "@babel/preset-typescript",
  "@babel/preset-react"
]

const plugins = [
  "babel-plugin-styled-components",
  "@babel/plugin-proposal-numeric-separator"
]

module.exports = { presets, plugins }
