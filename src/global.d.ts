declare module "typography-theme-noriega" {
  import { TypographyOptions } from "typography"

  const opts: TypographyOptions
  export default opts
}

declare module "react-typography"

declare module "*.svg" {
  const content: string
  export default content
}
