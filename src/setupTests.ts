import "jest-enzyme"
import "jest-styled-components"

window.matchMedia =
  window.matchMedia ||
  (() => {
    return {
      addListener: () => undefined,
      matches: false,
      media: "",
      removeListener: () => undefined
    }
  })
