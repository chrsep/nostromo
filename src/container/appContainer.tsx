import React, {
  ChangeEventHandler,
  FunctionComponent,
  useState,
  KeyboardEventHandler
} from "react"
import Header from "../components/header/header"
import MainInput from "../components/mainInput/mainInput"
import ResultContainer from "./resultContainer"
import MainContent from "../components/mainContent/mainContent"

const AppContainer: FunctionComponent = () => {
  const [state, setState] = useState({ inputValue: "", usedInputValue: "" })

  const handleInputChange: ChangeEventHandler<HTMLInputElement> = event => {
    setState({ ...state, inputValue: event.target.value })
  }
  const handleEnterKeypress: KeyboardEventHandler<HTMLInputElement> = event => {
    if (event.charCode === 13) {
      setState({ ...state, usedInputValue: state.inputValue })
    }
  }

  return (
    <>
      <MainContent>
        <Header>DENOMINATOR</Header>
        <MainInput
          onChange={handleInputChange}
          onKeyPress={handleEnterKeypress}
        />
        <ResultContainer input={state.usedInputValue} />
      </MainContent>
    </>
  )
}

export default AppContainer
