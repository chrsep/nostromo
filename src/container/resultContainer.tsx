import React, { FunctionComponent, useMemo, memo } from "react"
import processInput from "../utils/processInput"
import calculateDenominations from "../utils/calculateDenomination"

import ResultViewer from "../components/resultViewer/resultViewer"
import WelcomeView from "../components/welcomeView/welcomeView"
import ErrorView from "../components/errorView/errorView"

interface Props {
  input: string
}
const ResultContainer: FunctionComponent<Props> = memo(({ input }) => {
  // process and validate input
  const parsedInput = useMemo(() => processInput(input), [input])

  // calculate result
  const calculatedResult = useMemo(() => {
    if (parsedInput.isValid) {
      return calculateDenominations(parsedInput.value)
    }
    return []
  }, [parsedInput])

  // choose which view to render
  if (input === "") {
    return <WelcomeView />
  }
  if (parsedInput.isValid) {
    return <ResultViewer results={calculatedResult} input={parsedInput.value} />
  }
  return <ErrorView message={parsedInput.message} />
})

export default ResultContainer
