import { createGlobalStyle } from "styled-components"
import { normalize } from "styled-normalize"

const GlobalStyle = createGlobalStyle`
    ${normalize}
    html {
        background-color: #121212;
        overflow-y: auto !important;
    }
`

export default GlobalStyle
