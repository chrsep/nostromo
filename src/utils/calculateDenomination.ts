// All possible Rupiah's denominations
const defaultDenominations = [
  100_000,
  50_000,
  20_000,
  10_000,
  5_000,
  2_000,
  1_000,
  500,
  200,
  100,
  50
]

export interface ResultType {
  quantity: number
  denomination: number
  isLeftOver: boolean
}

export default function denominationCounter(
  amount: number,
  denominations = defaultDenominations
): ResultType[] {
  // Default values, returned if amount can't be represented by any denomination
  let denomination = amount
  let isLeftOver = true
  let quantity = 1

  // Find biggest denomination that is no bigger than the amount
  const closestDenomination = denominations.find(value => value <= amount)
  if (closestDenomination) {
    denomination = closestDenomination
    isLeftOver = false
    quantity = Math.floor(amount / denomination)
  }

  // Prepare result and plan the next step
  const result = [{ denomination, quantity, isLeftOver }]
  const amountLeft = amount - denomination * quantity
  if (isLeftOver || amountLeft === 0) {
    // Pack the bag and go home ...
    return result
  }
  // Continue searching smaller denomination
  const nextDenomination = denominationCounter(
    amountLeft,
    // Search only on smaller denominations than current one
    denominations.filter(item => item < denomination)
  )
  Array.prototype.push.apply(result, nextDenomination)

  return result
}
