import denominationCounter from "./calculateDenomination"

describe.each([
  // Test cases
  [1],
  [8],
  [70],
  [200],
  [800],
  [1_000],
  [1_500],
  [10_000],
  [14_543],
  [15_000],
  [10_800],
  [12_510],
  [110_910],
  [520_010],
  [1_438_953],
  [5_483_000],
  [200_000],
  [554_432_543]
])("calculateDenomination", (input: number) => {
  it(`split "${input}" correctly`, () => {
    const result = denominationCounter(input)
    expect(result).toMatchSnapshot()
  })

  it(`output total amount of "${input}"`, () => {
    const result = denominationCounter(input)
    const addedUpValue = result.reduce(
      (prev, curr) => prev + curr.denomination * curr.quantity,
      0
    )
    expect(addedUpValue).toEqual(input)
  })
})

describe("denominationCounter", () => {
  it(`return correct format`, () => {
    const input = 1051

    const result = denominationCounter(input)

    expect(result).toHaveLength(3)
    expect(result[0].denomination).toEqual(1000)
    expect(result[0].quantity).toEqual(1)
    expect(result[0].isLeftOver).toEqual(false)

    expect(result[1].denomination).toEqual(50)
    expect(result[1].quantity).toEqual(1)
    expect(result[1].isLeftOver).toEqual(false)

    expect(result[2].denomination).toEqual(1)
    expect(result[2].quantity).toEqual(1)
    expect(result[2].isLeftOver).toEqual(true)
  })
})
