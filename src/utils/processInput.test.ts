import processInput from "./processInput"

describe("processInput", () => {
  const validInputTest = (input: string, expected: number): void => {
    it("accepts and process valid input correctly", () => {
      const result = processInput(input)
      expect(result).toEqual({
        input,
        isValid: true,
        value: expected
      })
    })
  }

  describe(`accept unformatted number`, () => {
    const input = "233333"
    const expected = 233_333
    validInputTest(input, expected)
  })

  describe.each([["123.000", 123_000], ["123.123.123", 123_123_123]])(
    `accept dot as separator`,
    (input: string, expected: number) => {
      validInputTest(input, expected)
    }
  )

  describe.each([["Rp90000", 90_000], ["Rp4.000", 4_000]])(
    `accept "Rp" at the beginning of input`,
    (input: string, expected: number) => {
      validInputTest(input, expected)
    }
  )

  describe.each([
    ["150,00", 150],
    ["1.500,00", 1_500],
    ["Rp8.500,00", 8_500],
    ["Rp1.328.500,00", 1_328_500],
    ["Rp800,00", 800]
  ])(`accept comma as decimal indicator`, (input: string, expected: number) => {
    validInputTest(input, expected)
  })

  describe.each([
    ["000800", 800],
    ["00400.000", 400_000],
    ["0434.000,00", 434_000],
    ["0000000000050000,00", 50_000]
  ])(
    `disregards "0" on beginning of input`,
    (input: string, expected: number) => {
      validInputTest(input, expected)
    }
  )

  const invalidInputTest = (input: string): void => {
    it("reject invalid input", () => {
      const result = processInput(input)
      expect(result.isValid).toEqual(false)
      if (!result.isValid) expect(result.message).toMatchSnapshot()
    })
  }

  describe.each(["2Rp000", "2Rp.000,00", "2000Rp", "2000,00Rp", "200.000Rp"])(
    `reject "Rp" in places other than the start of input (%s)`,
    input => {
      invalidInputTest(input)
    }
  )

  describe.each([["1 000 000"], ["1,000"], ["1 000"], ["1,000,000"]])(
    `reject spaces and comma as separator (%s)`,
    input => {
      invalidInputTest(input)
    }
  )

  describe(`reject "Rp" without any number`, () => {
    const input = "Rp"
    invalidInputTest(input)
  })

  describe.each(["RP 1.000", "Rp 500", "RP1000", "rp100", "rp 1000"])(
    `reject "RP", "rp" and "Rp ", coz that's not how us literate patriotic indonesian denotes our currency (%s)`,
    input => {
      invalidInputTest(input)
    }
  )

  describe.each([
    "100a00",
    "230.00",
    "fd0384",
    "Rp.asdfa",
    "g2939f",
    "1_000",
    "324432gg",
    "324.g32",
    "Rpd2323",
    "Rp323d323",
    "00a003232",
    "00.000323",
    "Rp000323",
    "Rp00.0323",
    "2134.323.323",
    "123.3123.329"
  ])(
    ` reject random placement of dot, comma, and invalid characters (%s)`,
    input => {
      invalidInputTest(input)
    }
  )
})
