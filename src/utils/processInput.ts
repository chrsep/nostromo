export interface Valid {
  isValid: true
  input: string
  value: number
}

export interface Invalid {
  isValid: false
  input: string
  message: string
}

export type MaybeValid = Valid | Invalid

// find a good, useful error message for user
function generateErrorMessage(input: string): string {
  const stillHaveRp = input.toLowerCase().indexOf("rp") !== -1
  const stillHaveComma = input.indexOf(",") !== -1
  const stillHaveDot = input.indexOf(".") !== -1
  const stillHaveSpace = input.indexOf(" ") !== -1
  if (input === "")
    return `You are not giving us any useful value to work with, try input some number (eg. Rp1.000, 1000, etc...)`
  if (stillHaveRp)
    return `Wrong usage of currency symbol, correct usage is only "Rpxxxxx", do not use "rpxxxx", "RPxxxx", "Rp xxxx", "Rp.xxxxx", "rp xxxx", "xxxx Rp", etc..`
  if (stillHaveComma)
    return `Wrong usage of comma, use only for decimal (eg. Rp100.000,00), do not use as separator.`
  if (stillHaveDot)
    return `Wrong usage of dot, use only for separator (eg. Rp100.000), do not use as anything else such as Rp.1.000 or Rp100.00`
  if (stillHaveSpace)
    return `Did you use space? Space is not allowed as separator, use dot for separator (eg. 1.000)`
  return `There are invalid character in your input, only valid non-decimal number and rupiah currency format are allowed (eg. Rp1000, Rp1.000, Rp1.000,00 1.000, 1000, ect... )`
}

function validate(input: string): MaybeValid {
  let cleanedInput = input

  // Remove valid zeros on beginning of input
  const startsWithZero = input.indexOf("0") === 0
  if (startsWithZero) cleanedInput = input.replace(/0*/, "")

  // Remove valid "Rp" in input
  const startsWithRp = input.indexOf("Rp") === 0
  if (startsWithRp) cleanedInput = input.substring(2)

  // Remove valid decimals from input
  const splitByComma = cleanedInput.split(/,/g)
  if (splitByComma.length === 2 && splitByComma[1].length === 2)
    [cleanedInput] = splitByComma

  // Check if separators are placed in the correct spots
  const splitByDot = cleanedInput.split(/\./g)
  const isSeparatorValid = splitByDot.reduce((acc, item, idx) => {
    if (idx === 0) {
      return item.length <= 3 && acc
    }
    return item.length === 3 && acc
  }, true)
  if (isSeparatorValid) cleanedInput = splitByDot.join("")

  const onlyNumbers = cleanedInput
    .split("")
    .reduce((acc, char) => !Number.isNaN(parseInt(char, 10)) && acc, true)

  // Validate result and find error message if errors out
  const isValid = onlyNumbers && cleanedInput !== "" && cleanedInput[0] !== "0"
  if (isValid) {
    return { input, isValid, value: -1 }
  }
  const message = generateErrorMessage(cleanedInput)
  return { input, isValid, message }
}

export default function processInput(input: string): MaybeValid {
  const result = validate(input)
  if (result.isValid) {
    const cleanedInput = input.replace(/\./g, "").replace("Rp", "")
    result.value = parseInt(cleanedInput, 10)
  }
  return result
}
