import { storiesOf } from "@storybook/react"
import React from "react"
import { Box, Card, Flex, Text } from "rebass"
import VerticalDivider from "./verticalDivider"

storiesOf("VerticalDivider", module).add("default", () => (
  <Card m={3} backgroundColor="#212121">
    <Flex>
      <Text css={{ display: "inline" }} color="white" m={4} fontSize={4}>
        Title
      </Text>
      <Box>
        <VerticalDivider />
      </Box>
      <Text css={{ display: "inline" }} color="white" m={4} fontSize={4}>
        Title
      </Text>
    </Flex>
  </Card>
))
