import { mount } from "enzyme"
import React from "react"
import VerticalDivider from "./verticalDivider"

describe("VerticalDivider Component", () => {
  it("renders correcyly", () => {
    expect(mount(<VerticalDivider />)).toMatchSnapshot()
  })
})
