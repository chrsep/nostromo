import { Box as Base } from "rebass"
import styled from "styled-components"

const Divider = styled(Base).attrs({
  as: "hr",
  bg: "grey"
})`
  border: 0;
  opacity: 0.2;
  width: 1px;
  height: 100%;
`

export default Divider
