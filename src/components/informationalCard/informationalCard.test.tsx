import { mount, shallow } from "enzyme"
import React from "react"
import InfoIcon from "../../assets/info.svg"
import InformationalCard from "./informationalCard"

describe("InformationalCard Component", () => {
  const message =
    "Starting a company is like eating glass and staring into the abyss.' You have to do lots of things you don't like."

  it("renders correctly", () => {
    expect(
      mount(
        <InformationalCard
          backgroundColor="white"
          headerColor="black"
          headerIcon={InfoIcon}
          headerText="Test"
          textColor="black"
          headerIconAlt="info icon"
        >
          {message}
        </InformationalCard>
      )
    ).toMatchSnapshot()
  })

  it("renders given message", () => {
    const wrapper = shallow(
      <InformationalCard
        backgroundColor="white"
        headerColor="black"
        headerIcon={InfoIcon}
        headerText="Test"
        textColor="black"
        headerIconAlt="info icon"
      >
        {message}
      </InformationalCard>
    )
    expect(wrapper).toIncludeText(message)
  })
})
