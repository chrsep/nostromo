import { text } from "@storybook/addon-knobs"
import { storiesOf } from "@storybook/react"
import React from "react"
import { Box } from "rebass"
import InfoIcon from "../../assets/info.svg"
import InformationalCard from "./informationalCard"

const message = `Starting a company is like eating glass and staring into the abyss. You have to do lots of things you dont like.`

storiesOf("InformationalCard", module).add("default", () => (
  <Box m={3}>
    <InformationalCard
      backgroundColor={text("Background Color", "white")}
      headerColor={text("Header Color", "black")}
      headerIcon={InfoIcon}
      headerText={text("Header Text", "Information")}
      textColor={text("Text Color", "black")}
      headerIconAlt="info icon"
    >
      {text("message", message)}
    </InformationalCard>
  </Box>
))
