import React, { FunctionComponent } from "react"
import { Card, Flex, Image, Text } from "rebass"

interface Props {
  textColor: string
  headerText: string
  headerIcon: string
  headerColor: string
  backgroundColor: string
  headerIconAlt: string
}
const InformationalCard: FunctionComponent<Props> = ({
  backgroundColor,
  headerColor,
  headerIcon,
  headerText,
  textColor,
  headerIconAlt,
  children
}) => (
  <Card p={3} backgroundColor={backgroundColor} borderRadius={4}>
    <Flex mb={2} alignItems="center">
      <Image width="18px" src={headerIcon} alt={headerIconAlt} />
      <Text pl={2} color={headerColor} fontSize={1}>
        <b>{headerText}</b>
      </Text>
    </Flex>
    <Text color={textColor} fontSize={1}>
      {children}
    </Text>
  </Card>
)

export default InformationalCard
