import { Flex, Box } from "rebass"
import React, { FunctionComponent } from "react"

const MainContent: FunctionComponent = ({ children }) => (
  <Flex flexDirection="column" alignItems="center">
    <Box as="main" css={{ maxWidth: 900 }} width="100%">
      {children}
    </Box>
  </Flex>
)

export default MainContent
