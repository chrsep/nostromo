import { storiesOf } from "@storybook/react"
import React from "react"
import { Box } from "rebass"
import MainContent from "./mainContent"

storiesOf("MainContent", module).add("default", () => (
  <MainContent>
    <Box backgroundColor="white" width="100%" />
  </MainContent>
))
