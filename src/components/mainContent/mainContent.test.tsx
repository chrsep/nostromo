import React from "react"
import { mount } from "enzyme"
import MainContent from "./mainContent"

describe("MainContent Component", () => {
  it("renders correctly", () => {
    expect(mount(<MainContent>TEST</MainContent>)).toMatchSnapshot()
  })

  it("renders its children", () => {
    const children = "Je suis seulement un enfant"
    const wrapper = mount(<MainContent>{children}</MainContent>)
    expect(wrapper).toIncludeText(children)
  })
})
