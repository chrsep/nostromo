import { mount } from "enzyme"
import React from "react"
import WelcomeView from "./welcomeView"

describe("WelcomeView Component", () => {
  it("renders correctly", () => {
    expect(mount(<WelcomeView />)).toMatchSnapshot()
  })
})
