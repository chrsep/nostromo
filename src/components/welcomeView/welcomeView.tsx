import { Box } from "rebass"
import React, { FunctionComponent } from "react"
import Info from "../info/info"

const WelcomeView: FunctionComponent = () => (
  <Box m={3} data-cy="welcome-instruction">
    <Info>
      <p>
        Welcome, start typing on input field above to start. Example of valid
        inputs are:
      </p>
      <Box as="ul" mb={0}>
        <li>1000</li>
        <li>1.000</li>
        <li>1.000,00</li>
        <li>Rp1.000,00</li>
      </Box>
    </Info>
  </Box>
)

export default WelcomeView
