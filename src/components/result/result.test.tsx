import { mount } from "enzyme"
import React from "react"
import Result from "./result"

describe("Result Component", () => {
  const denomination = 50_000
  const quantity = 1
  it("renders correctly", () => {
    expect(
      mount(<Result denomination={denomination} quantity={quantity} />)
    ).toMatchSnapshot()
  })

  it("renders given denomination correctly", () => {
    const wrapper = mount(
      <Result denomination={denomination} quantity={quantity} />
    )
    expect(wrapper).toIncludeText(`Rp${denomination.toLocaleString("id")}`)
  })

  it("renders given quantity correctly", () => {
    const wrapper = mount(
      <Result denomination={denomination} quantity={quantity} />
    )
    expect(wrapper).toIncludeText(quantity.toString())
  })
})
