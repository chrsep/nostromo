import React, { FunctionComponent } from "react"
import { Card, Flex, Image, Text } from "rebass"
import XIcon from "../../assets/x.svg"

interface Props {
  denomination: number
  quantity: number
  isLeftOver: boolean
}

const Result: FunctionComponent<Props> = ({
  quantity,
  denomination,
  isLeftOver
}) => (
  <Flex data-cy="result">
    <Card backgroundColor="green" borderRadius={4}>
      <Text
        p={!isLeftOver ? 3 : 0}
        px={!isLeftOver ? 4 : 0}
        color="white"
        fontSize={3}
      >
        <b>{!isLeftOver && quantity}</b>
      </Text>
    </Card>
    <Card
      ml={!isLeftOver ? 3 : 0}
      width="100%"
      borderRadius={4}
      backgroundColor={!isLeftOver ? "#212121" : "green"}
    >
      <Flex>
        {!isLeftOver && (
          <Image ml={3} src={XIcon} alt="multiplication symbol" />
        )}
        {isLeftOver && (
          <Text p={3} color="white" fontSize={3}>
            Left Over
          </Text>
        )}
        <Text p={3} color="white" fontSize={3}>
          <b>Rp{denomination.toLocaleString("id")}</b>
        </Text>
      </Flex>
    </Card>
  </Flex>
)

export default Result
