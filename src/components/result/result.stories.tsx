import { number } from "@storybook/addon-knobs"
import { storiesOf } from "@storybook/react"
import React from "react"
import { Box } from "rebass"
import Result from "./result"

const denomination = 50_000
const quantity = 1
storiesOf("Result", module).add("default", () => (
  <Box m={3}>
    <Result
      denomination={number("Denomination", denomination)}
      quantity={number("quantity", quantity)}
    />
  </Box>
))
