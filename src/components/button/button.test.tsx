import { mount } from "enzyme"
import React from "react"
import Button from "./button"

describe("Button Component", () => {
  it("render correctly", () => {
    expect(mount(<Button />)).toMatchSnapshot()
  })

  it("handle onClick", () => {
    const callback = jest.fn()
    const wrapper = mount(<Button onClick={callback} />)
    wrapper.find("button").simulate("click")
    expect(callback).toHaveBeenCalled()
  })

  it("render given text", () => {
    const text = "DETONATE"
    const wrapper = mount(<Button>{text}</Button>)
    expect(wrapper).toIncludeText(text)
  })
})
