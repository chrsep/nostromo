import React, { FunctionComponent } from "react"
import { Button as Base, ButtonProps } from "rebass"

const Button: FunctionComponent<ButtonProps> = props => (
  <Base
    {...props}
    backgroundColor="black"
    borderRadius={4}
    border="none"
    color="white"
    css={{
      boxShadow: "0 0 0 1px rgba(0, 0, 0, 0.1), 0 2px 4px rgba(0, 0, 0, 0.1)",
      textTransform: "uppercase"
    }}
  />
)

export default Button
