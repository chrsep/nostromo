import { storiesOf } from "@storybook/react"
import React from "react"
import { action } from "@storybook/addon-actions"
import Button from "./button"

storiesOf("Button", module)
  .addParameters({
    backgrounds: [{ name: "White", value: "white", default: true }]
  })
  .add("default", () => (
    <div>
      <Button onClick={action("Clicked")}>Click Me</Button>
    </div>
  ))
