import { mount, shallow } from "enzyme"
import React from "react"
import { ResultType } from "../../utils/calculateDenomination"
import ResultViewer from "./resultViewer"
import Result from "../result/result"

describe("ResultViewer Component", () => {
  const results: ResultType[] = [
    {
      quantity: 2,
      denomination: 100_000,
      isLeftOver: false
    },
    {
      quantity: 1,
      denomination: 50_000,
      isLeftOver: false
    },
    {
      quantity: 1,
      denomination: 123,
      isLeftOver: true
    }
  ]
  it("renders correctly", () => {
    expect(
      mount(<ResultViewer input={250123} results={results} />)
    ).toMatchSnapshot()
  })

  it("render all result", () => {
    const wrapper = shallow(<ResultViewer input={250123} results={results} />)
    expect(wrapper.find(Result)).toHaveLength(results.length)
  })
})
