import { storiesOf } from "@storybook/react"
import React from "react"
import { ResultType } from "../../utils/calculateDenomination"
import ResultViewer from "./resultViewer"

storiesOf("ResultViewer", module).add("default", () => {
  const results: ResultType[] = [
    {
      quantity: 2,
      denomination: 100_000,
      isLeftOver: false
    },
    {
      quantity: 1,
      denomination: 50_000,
      isLeftOver: false
    },
    {
      quantity: 1,
      denomination: 123,
      isLeftOver: true
    }
  ]

  return <ResultViewer input={250123} results={results} />
})
