import React, { FunctionComponent } from "react"
import { Box, Text } from "rebass"
import { ResultType } from "../../utils/calculateDenomination"
import Result from "../result/result"

interface Props {
  results: ResultType[]
  input: number
}
const ResultViewer: FunctionComponent<Props> = ({ input, results }) => (
  <Box>
    <Text m={3} color="rgba(255,255,255,0.6)" as="h6" fontSize={2}>
      Rp{input.toLocaleString("id")} can be represented as:
    </Text>
    {results.map(result => (
      <Box m={3} key={result.denomination}>
        <Result
          isLeftOver={result.isLeftOver}
          denomination={result.denomination}
          quantity={result.quantity}
        />
      </Box>
    ))}
  </Box>
)

export default ResultViewer
