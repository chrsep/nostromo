import { mount } from "enzyme"
import React from "react"
import Header from "./header"

describe("Header Component", () => {
  it("render correctly", () => {
    expect(mount(<Header>DENOMINATOR</Header>)).toMatchSnapshot()
  })

  it("render given text", () => {
    const title = "DENOMINATOR"
    const wrapper = mount(<Header>{title}</Header>)
    expect(wrapper).toIncludeText(title)
  })
})
