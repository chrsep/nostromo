import React, { FunctionComponent } from "react"
import { Text } from "rebass"

const Header: FunctionComponent = ({ children }) => (
  <Text
    letterSpacing={6}
    p={3}
    mb={2}
    width="100%"
    textAlign="center"
    color="white"
    as="h1"
    css={{whiteSpace: "nowrap"}}
  >
    {children}
  </Text>
)

export default Header
