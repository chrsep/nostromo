import { text } from "@storybook/addon-knobs"
import { storiesOf } from "@storybook/react"
import React from "react"
import Header from "./header"

storiesOf("Header", module).add("default", () => (
  <Header>{text("text", "DENOMINATOR")}</Header>
))
