import React from "react"
import { storiesOf } from "@storybook/react"
import ErrorView from "./errorView"

const message =
  "What makes you think she is a witch?  She turned me into a newt"
storiesOf("WelcomeView", module).add("default", () => (
  <ErrorView message={message} />
))
