import React, { FunctionComponent } from "react"
import { Box } from "rebass"
import Warning from "../warning/warning"

interface Props {
  message: string
}
const ErrorView: FunctionComponent<Props> = ({ message }) => (
  <Box m={3} data-cy="error">
    <Warning>{message}</Warning>
  </Box>
)

export default ErrorView
