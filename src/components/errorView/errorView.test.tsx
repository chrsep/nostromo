import { mount } from "enzyme"
import React from "react"
import ErrorView from "./errorView"

describe("WelcomeView Component", () => {
  const message =
    "What makes you think she is a witch?  She turned me into a newt"
  it("renders correctly", () => {
    expect(mount(<ErrorView message={message} />)).toMatchSnapshot()
  })
})
