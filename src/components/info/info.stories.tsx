import { storiesOf } from "@storybook/react"
import React from "react"
import { Box } from "rebass"
import Info from "./info"

const message =
  "An old friend once told me something that gave me great comfort. Something he had read. He said that Mozart, Beethoven, and Chopin never died. They simply became music."

storiesOf("Info", module).add("default", () => (
  <Box m={3}>
    <Info>{message}</Info>
  </Box>
))
