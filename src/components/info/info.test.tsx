import { mount } from "enzyme"
import React from "react"
import Info from "./info"

describe("Info Component", () => {
  const message =
    "An old friend once told me something that gave me great comfort. Something he had read. He said that Mozart, Beethoven, and Chopin never died. They simply became music."
  it("renders correctly", () => {
    expect(mount(<Info>{message}</Info>)).toMatchSnapshot()
  })

  it("renders given message", () => {
    const wrapper = mount(<Info>{message}</Info>)
    expect(wrapper).toIncludeText(message)
  })
})
