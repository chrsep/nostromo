import React, { FunctionComponent } from "react"
import InfoIcon from "../../assets/info.svg"
import InformationalCard from "../informationalCard/informationalCard"

const Info: FunctionComponent = ({ children }) => (
  <InformationalCard
    backgroundColor="#212121"
    headerColor="#00DB04"
    headerIcon={InfoIcon}
    headerText="Information"
    textColor="white"
    headerIconAlt="info icon"
  >
    {children}
  </InformationalCard>
)

export default Info
