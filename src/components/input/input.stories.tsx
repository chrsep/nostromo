import { text } from "@storybook/addon-knobs"
import { storiesOf } from "@storybook/react"
import React from "react"
import { Box } from "rebass"
import Input from "./input"

storiesOf("Input", module).add("default", () => (
  <Box m={3}>
    <Input
      aria-label="random"
      placeholder={text("placholder", "Type something")}
    />
  </Box>
))
