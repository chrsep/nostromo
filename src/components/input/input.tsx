import { Box } from "rebass"
import styled from "styled-components"

const Input = styled(Box).attrs({
  fontSize: 2,
  as: "input",
  p: 3
})`
  /* allows overriding padding left for putting icons or something in front*/
  ${({ pl }) => pl && `paddling-left:${pl} !important;`}
  border: none;
  outline: none;
  border-radius: 4px;
  color: white;
  width: 100%;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1), 0 2px 4px rgba(0, 0, 0, 0.1);
  background-color: #212121;
`

export default Input
