import { mount } from "enzyme"
import React from "react"
import Input from "./input"

describe("Input Component", () => {
  it("render correctly", () => {
    expect(mount(<Input />)).toMatchSnapshot()
  })
  it("handle on change", () => {
    const callback = jest.fn()
    const wrapper = mount(<Input onChange={callback} />)
    wrapper.find("input").simulate("change")
    expect(callback).toHaveBeenCalled()
  })

  it("handle enter key press", () => {
    const callback = jest.fn()
    const wrapper = mount(<Input onKeyPress={callback} />)
    wrapper.find("input").simulate("keyPress")
    expect(callback).toHaveBeenCalled()
  })
})
