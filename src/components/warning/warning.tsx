import React, { FunctionComponent } from "react"
import AlertIcon from "../../assets/alert-octagon.svg"
import InformationalCard from "../informationalCard/informationalCard"

const Warning: FunctionComponent = ({ children }) => (
  <InformationalCard
    backgroundColor="#AD0006 "
    headerColor="white"
    headerIcon={AlertIcon}
    headerText="Warning"
    textColor="white"
    headerIconAlt="alert icon"
  >
    {children}
  </InformationalCard>
)

export default Warning
