import { storiesOf } from "@storybook/react"
import React from "react"
import { Box } from "rebass"
import Warning from "./warning"

const message = "Sometimes to love someone, you got to be a stranger."

storiesOf("Warning", module).add("default", () => (
  <Box m={3}>
    <Warning>{message}</Warning>
  </Box>
))
