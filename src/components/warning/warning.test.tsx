import { mount, shallow } from "enzyme"
import React from "react"
import Warning from "./warning"

describe("WarningCard", () => {
  const message = "Sometimes to love someone, you got to be a stranger."

  it("renders correctly", () => {
    expect(mount(<Warning>{message}</Warning>)).toMatchSnapshot()
  })

  it("renders given message", () => {
    const wrapper = shallow(<Warning>{message}</Warning>).dive()
    expect(wrapper).toIncludeText(message)
  })
})
