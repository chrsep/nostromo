import React, {
  ChangeEventHandler,
  FunctionComponent,
  KeyboardEventHandler,
  memo
} from "react"
import { Box, Image } from "rebass"
import EditIcon from "../../assets/edit.svg"
import Input from "../input/input"

interface Props {
  onChange?: ChangeEventHandler<HTMLInputElement>
  onKeyPress?: KeyboardEventHandler<HTMLInputElement>
}
const MainInput: FunctionComponent<Props> = memo(({ onChange, onKeyPress }) => (
  <Box p={3} pb={0} width="100%">
    <Image
      alt="edit icon"
      src={EditIcon}
      p={3}
      css={{
        position: "absolute",
        stroke: "white",
        opacity: 0.45,
        pointerEvents: "none"
      }}
    />
    <Input
      data-cy="main-input"
      pl="56px"
      aria-label="main input"
      placeholder="Type a value (eg. 100000)"
      onChange={onChange}
      onKeyPress={onKeyPress}
    />
  </Box>
))

export default MainInput
