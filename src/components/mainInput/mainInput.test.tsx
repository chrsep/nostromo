import { mount } from "enzyme"
import React from "react"
import MainInput from "./mainInput"

describe("MainInput Component", () => {
  it("render correctly", () => {
    expect(mount(<MainInput />)).toMatchSnapshot()
  })
  it("handle on change", () => {
    const callback = jest.fn()
    const wrapper = mount(<MainInput onChange={callback} />)
    wrapper.find("input").simulate("change")
    expect(callback).toHaveBeenCalled()
  })

  it("handle enter key press", () => {
    const callback = jest.fn()
    const wrapper = mount(<MainInput onKeyPress={callback} />)
    wrapper.find("input").simulate("keyPress")
    expect(callback).toHaveBeenCalled()
  })
})
