import React from "react"
import ReactDOM from "react-dom"
import { GoogleFont, TypographyStyle } from "react-typography"
import AppContainer from "./container/appContainer"
import GlobalStyle from "./globalStyle"
import typography from "./typography"

// Setup tooling,libraries and global styles
ReactDOM.render(
  <>
    <TypographyStyle typography={typography} />
    <GoogleFont typography={typography} />
    <GlobalStyle />
    <AppContainer />
  </>,
  document.getElementById("app")
)
