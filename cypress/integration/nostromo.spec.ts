describe("Nostromo", () => {
  beforeEach(() => {
    cy.visit("http://localhost:8080")
  })

  it("shows some instruction to user when they first arrives", () => {
    cy.get("[data-cy=welcome-instruction]")
  })

  it("useable main input is available right away", () => {
    cy.get("[data-cy=main-input]")
  })

  it("does nothing until user press enter.", () => {
    cy.get("[data-cy=main-input]").type("deckard")

    cy.get("[data-cy=welcome-instruction]")
  })

  it("shows error when given invalid input", () => {
    cy.get("[data-cy=main-input]").type("wallace{enter}")

    cy.get("[data-cy=error]")
  })

  it("it shows the results when givin a correct input", () => {
    cy.get("[data-cy=main-input]").type("Rp1.000.010{enter}")

    cy.get("[data-cy=result]")
  })

  it("updated result when input changed and user press enter", () => {
    // enter valid input
    cy.get("[data-cy=welcome-instruction]")
    cy.get("[data-cy=main-input]").type("Rp1{enter}")
    cy.get("[data-cy=result]")

    // should show error when valid input turns invalid
    cy.get("[data-cy=main-input]").type("{backspace}")
    cy.get("[data-cy=result]")
    cy.get("[data-cy=main-input]").type("{enter}")
    cy.get("[data-cy=error]")

    // when field is empty again, instruction is shown again
    cy.get("[data-cy=main-input]").type("{selectall}{backspace}")
    cy.get("[data-cy=error]")
    cy.get("[data-cy=main-input]").type("{enter}")
    cy.get("[data-cy=welcome-instruction]")
  })
})
