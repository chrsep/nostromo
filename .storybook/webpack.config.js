const path = require("path")
// 1. import default from the plugin module
// const createStyledComponentsTransformer = require('typescript-plugin-styled-components').default;

// 2. create a transformer;
// the factory additionally accepts an options object which described below
// const styledComponentsTransformer = createStyledComponentsTransformer();

// 3. add getCustomTransformer method to the loader config

module.exports = ({ config }) => {
  // Config for parsing typescript=================================================
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    include: path.resolve(__dirname, "../src"),
    use: [
      {
        loader: "babel-loader"
      }
    ]
  })
  // Speed up storybook compilation===============================================
  config.optimization = {
    ...config.optimization,
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false
  }
  config.output = {
    ...config.output,
    pathinfo: false
  }

  config.resolve.extensions.push(".ts", ".tsx")

  return config
}
