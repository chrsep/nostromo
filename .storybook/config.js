import { addDecorator, addParameters, configure } from "@storybook/react"
import { themes } from "@storybook/theming"
import React from "react"
import { withKnobs } from "@storybook/addon-knobs"
import { action } from "@storybook/addon-actions"
import GlobalStyle from "../src/globalStyle"
import typography from "../src/typography"
import { GoogleFont, TypographyStyle } from "react-typography"
import { withA11y } from '@storybook/addon-a11y';

const reqComponents = require.context(
  "../src/components",
  true,
  /\.stories\.tsx$/
)

function loadComponents() {
  reqComponents.keys().forEach(filename => reqComponents(filename))
}

const TypographyDecorator = story => (
  <>
    <TypographyStyle typography={typography} />
    <GoogleFont typography={typography} />
    <GlobalStyle />
    {story()}
  </>
)
addDecorator(TypographyDecorator)
addDecorator(withKnobs)
addDecorator(withA11y)

addParameters({
  options: {
    name: "Nostromo",
    theme: themes.dark
  }
})

addParameters({
  backgrounds: [
    { name: "light", value: "#ffffff" },
    { name: "dark", value: "#121212", default: true }
  ]
})

// Gatsby's Link overrides:
// Gatsby defines a global called ___loader to prevent its method calls from creating console errors you override it here
global.___loader = {
  enqueue: () => {},
  hovering: () => {}
}
// Gatsby internal mocking to prevent unnecessary errors in storybook testing environment
global.__PATH_PREFIX__ = ""
// This is to utilized to override the window.___navigate method Gatsby defines and uses to report what path a Link would be taking us to if it wasn't inside a storybook
window.___navigate = pathname => {
  action("NavigateTo:")(pathname)
}

configure(loadComponents, module)
