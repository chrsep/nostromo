# nostromo

[![pipeline status](https://gitlab.com/chrsep/nostromo/badges/master/pipeline.svg)](https://gitlab.com/chrsep/nostromo/commits/master)
[![coverage report](https://gitlab.com/chrsep/nostromo/badges/master/coverage.svg)](https://gitlab.com/chrsep/nostromo/commits/master)

Experiment with react.

production build: [now](https://nostromo.chrsep.now.sh/)
storybook: [now](https://nostromo-storybook.chrsep.now.sh/)

## Requirements

1. Nodejs
2. yarn / npm

## How to run

1. clone the repo, `git clone https://gitlab.com/chrsep/nostromo.git`
2. install dependencies, `yarn` or `npm install` .
3. `yarn develop` or `npm run develop` to run development build on `localhost:8080`.

### Other commands

1. `yarn storybook` or `npm run storybook` to run [storybook](https://storybook.js.org/).
2. `yarn build` or `npm run build` to build production build, output in `public`.
3. `yarn test` or `npm run test` to run unit tests using jest.
4. `yarn test:cypress` or `npm run test:cypress` to run cypress for integration tests.

## Structure

The following details how the project is structured, all source code lives in `src` folder. Unit tests lives in `src` along side each tested code, integration tests lives in `cypress/integration` on root.

### Presentational Components (`src/components/`)

Cares only about styling, business logic should be avoided from being placed here, this way we have clean separation between style and function.

### Container

Cares about logic and data handling. States lives only inside containers so that when we are debugging logic problems, we will only have to look on container components and disregards
the presentational components.

### Stories

Components are developed individually using storybook, this way it is easier to iterate on each components with little troubles from other components and states. Plus all components are now documented inside storybook. Stories lives side by side with the components they're about, in a file named `*.stories.tsx`.

### Utils

Complex enough algorithms that can be kept away from react are placed in utils, for ease of testing and decluttering of react components.

### Tests ([cypress](https://www.cypress.io/) & [jest](https://jestjs.io/))

Test are created using jest and cypress, jest unit tests live alongside the component it tests, this way, when modifying a single component we would only need to make changes in a single folder. Cypress integration tests lives on its own inside `cypress` folder in the root of the project.

## Tech Stack

### [Typescript](https://www.typescriptlang.org/)

Typescript provides type safety which gives more confidence on the code that are written. Avoiding small mistakes like typos easily, providing autocomplete, and access to documentations one click away.

### [Rebass](https://rebassjs.org/)

Rebass is a small component library consisting of 8 different comopnents, think of it as atoms in atomic design, a small indivisible components, that provides consistent API and restriction of choice, making it easier to make decision, while also providing full css styling capabilities using its css props and the `styled` component.

### Webpack & Babel

Used for transpiling typescript and bundling.
